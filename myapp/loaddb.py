import yaml



class Aide:

    def __init__(self, name, url, desc, id):
        self.id=id
        self.name=name
        self.url=url
        self.apport=[]
        self.desc=desc
        self.conditions=[]
        self.motcle=[]
        self.cas_particulie=[]
        self.cumulable=[]

    def ajout_apport(self, apport):
        self.apport.append(apport)

    def ajout_condition(self, condition):
        # if type(condition)==""
        self.conditions.append(condition)

    def ajout_motcle(self, motcle):
        self.motcle.append(motcle)

    def ajout_cas_particulie(self, cas):
        self.cas_particulie.append(cas)

    def ajout_commulable(self, cumulable):
        self.cumulable.append(cumulable)

    def __repr__(self):
        return self.name

class Condition:

    def __init__(self, name, desc):
        self.name=name
        self.desc=desc
        est_francais=None
        localisation=None

    def setFR(self, bool):
        self.est_francais=bool

    def setLocal(self, bool):
        self.localisation=bool

    def __repr__(self):
        return self.name

bd = yaml.load(open("static/bd.yml"))

list_aide=[]
def get_aide_byname(nomAide):
    global list_aide
    for a in list_aide:
        if a.name == nomAide:
            return a
    return False

def get_aide_by_id(id):
    global list_aide
    for a in list_aide:
        if a.id == id:
            return a
    return False

list_cond=[]
verif_list=[]
fini = False
while not fini:
    fini = True
    for aides in bd :
        a = Aide(aides["name"], aides["url"], aides["desc"], len(list_aide))
        list_aide.append(a)
        for apport in aides["apport"]:
            a.ajout_apport(apport)
        for motcle in aides["motcle"]:
            a.ajout_motcle(motcle)
        if "condition" in aides:
            for cond in aides["condition"]:
                c = Condition(cond["nom"], cond["desc"])
                if "est_fr" in cond:
                    c.setFR(cond["est_fr"])
                if "localisation" in cond:
                    c.setLocal(cond["localisation"])
                a.ajout_condition(c)
                list_cond.append(c)
        if "cumulable" in aides:
            for cumul in aides["cumulable"]:
                x = get_aide_byname(cumul)
                if x != False:
                    a.ajout_commulable(x)
                else:
                    verif_list.append(a)

    if len(verif_list)!=0:
        fini=False
    verif_list=[]

for aides in list_aide:
    print(aides.name)
    print(aides.url)
    print(aides.desc)
    print(aides.motcle)
    print(aides.cumulable)
    print(aides.conditions)
    print(aides.cas_particulie)
    print("__________________________")
