from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
import os.path

app = Flask(__name__)

app.config['BOOTSTRAP_SERVE_LOCAL'] = True


Bootstrap(app)


def mkpath(p):
    return os.path.normpath(os.path.join(os.path.dirname(__file__), p))

app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../myapp.db'))
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = "1cb8c4dc-daee-4c50-8afb-d13fb2e82a67"

from flask_login import LoginManager
login_manager = LoginManager(app)
